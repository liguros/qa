This Pipeline has two modes of testing:

- automatic desktop environment
- single package

Automatic desktop environment testing:
--------------------------------------
Testing if all major Desktop Environments build on develop version. This pipeline is run on schedule and tests most of the major DE.

Single package testing:
-----------------------
This can be used to test single packages with optional USE= variables.

How to start:

- Go to CI/CD - Pipelines
- Click "Run pipeline"
- Select correct branch under "Run for"
- Enter the needed variables 
  - SINGLE - sets to run single package test (if DE useflags are needed SINGLE_DE with the value of liguros mix-ins can be used)
  - PKG - package to test (either catpkg or package atom)
  - and optionally USE
- Click on "Run pipeline"
